#ifndef GRIDITEM_H
#define GRIDITEM_H

#include <QGraphicsObject>
#include <QPainter>
#include "Grid.h"

/*
    Class that can draw a grid to the screen
*/

const int BLOCK_WIDTH = 30;
const int BLOCK_HEIGHT = 30;

class GridItem : public QGraphicsObject
{

public:
    GridItem(Grid* grid = 0);

    void setGrid(Grid* grid);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;


private:
    Grid* grid;
    QColor* colors;
    QPoint offset;
};

#endif // TESTITEM_H

