#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "griditem.h"
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    piece = new FallingPiece();
    nextPiece = new FallingPiece();

    grid = new GameGrid();

    GridItem* item = new GridItem(grid);
    GridItem* item2 = new GridItem(piece);
    GridItem* item3 = new GridItem(nextPiece);

    item->setPos(0, 0);
    item2->setPos(0, 0);
     item3->setPos(20, 20);

    this->setFocusPolicy(Qt::StrongFocus);

    ui->graphicsView->setScene(new QGraphicsScene(0, 0, 300, 600));
    ui->graphicsView->scene()->addItem(item);
    ui->graphicsView->scene()->addItem(item2);

    ui->graphicsView_2->setScene(new QGraphicsScene(0, 0, 180, 180));
    ui->graphicsView_2->scene()->addItem(item3);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(tick()));
    timer->start(500);

    this->grabKeyboard();

    piece->generateRandom();
    nextPiece->generateRandom();


    this->ui->graphicsView_2->viewport()->update();

    score = 0;
    level = 1;
    gameState = 0;

    ui->label_3->hide();
}

MainWindow::~MainWindow()
{
    this->releaseKeyboard();
    delete ui;
}

bool MainWindow::event(QEvent *event)
{
    if(gameState != 0) QWidget::event(event);

    if(event->type() == QEvent::KeyPress)
    {
         QKeyEvent *ke = (QKeyEvent *)event;
         if (ke->key() == Qt::Key_Left)
         {
             piece->setOffsetX(piece->getOffsetX()-1);

             while(piece->checkLeft(grid) || grid->isPieceOverlapping(piece))
                piece->setOffsetX(piece->getOffsetX()+1);

             this->ui->graphicsView->viewport()->update();
             return true;
         }
         else if (ke->key() == Qt::Key_Right)
         {
             piece->setOffsetX(piece->getOffsetX()+1);

             while(piece->checkRight(grid) || grid->isPieceOverlapping(piece))
                piece->setOffsetX(piece->getOffsetX()-1);

             this->ui->graphicsView->viewport()->update();
             return true;
         }
         else if (ke->key() == Qt::Key_Down)
         {
             piece->setOffsetY(piece->getOffsetY()+1);

             while(piece->checkBottom(grid) || grid->isPieceOverlapping(piece))
                piece->setOffsetY(piece->getOffsetY()-1);

             this->ui->graphicsView->viewport()->update();
             return true;
         }

         else if (ke->key() == Qt::Key_Up)
         {
             piece->rotateRight();

             if(piece->checkLeft(grid) || piece->checkRight(grid) || piece->checkBottom(grid) || grid->isPieceOverlapping(piece))
                 piece->rotateLeft();

             this->ui->graphicsView->viewport()->update();
             return true;
         }
         else if (ke->key() == Qt::Key_Space)
         {
            while(!piece->checkBottom(grid) && !grid->isPieceOverlapping(piece))
                piece->setOffsetY(piece->getOffsetY()+1);

            piece->setOffsetY(piece->getOffsetY()-1);

            addPiece();

            this->ui->graphicsView_2->viewport()->update();
            this->ui->graphicsView->viewport()->update();

            return true;
        }

        return true;
    }

    return QWidget::event(event);
}

void MainWindow::addPiece()
{
    grid->addPiece(piece);

    int rows = grid->getFullRowCount();
    if(rows > 0) {
        grid->removeFullRows();
        score += 10 + ((rows-1) * 20);
        level += rows;

        this->ui->scoreLabel->setText(QString::number(score));
        this->ui->levelLabel->setText(QString::number((level / 10)+1));

        timer->setInterval(500-((level/10)+1)*50);
    }

    piece->copy(nextPiece);
    nextPiece->generateRandom();
    piece->setOffset(3, -2);

    if(grid->isPieceOverlapping(piece))
    {
        ui->label_3->show();
        gameState = 1;
    }
}

void MainWindow::tick()
{
    if(gameState != 0) return;

    piece->setOffsetY(piece->getOffsetY()+1);

    if(piece->checkBottom(grid) || grid->isPieceOverlapping(piece)) {
        piece->setOffsetY(piece->getOffsetY()-1);
        addPiece();
        this->ui->graphicsView_2->viewport()->update();
    }

    this->ui->graphicsView->viewport()->update();
}

void MainWindow::on_actionNew_game_triggered()
{
    timer->setInterval(500);

    piece->setOffset(3, -2);
    piece->generateRandom();
    nextPiece->generateRandom();
    score = 0;
    level = 1;
    gameState = 0;
    ui->label_3->hide();
    grid->clear();

    this->ui->graphicsView_2->viewport()->update();
    this->ui->graphicsView->viewport()->update();
}
