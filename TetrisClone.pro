#-------------------------------------------------
#
# Project created by QtCreator 2013-10-10T12:07:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TetrisClone
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    grid.cpp \
    gamegrid.cpp \
    griditem.cpp \
    fallingpiece.cpp

HEADERS  += mainwindow.h \
    grid.h \
    gamegrid.h \
    griditem.h \
    fallingpiece.h \
    PieceData.h

FORMS    += mainwindow.ui
