#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "gamegrid.h"
#include "fallingpiece.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool event(QEvent *event);

    void addPiece();

    ~MainWindow();

private slots:
    void tick();

    void on_actionNew_game_triggered();

private:
    Ui::MainWindow *ui;
    GameGrid* grid;
    FallingPiece* piece;
    FallingPiece* nextPiece;

    QTimer *timer;
    int score;
    int level;
    int gameState;
};

#endif // MAINWINDOW_H
