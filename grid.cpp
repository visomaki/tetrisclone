#include "grid.h"

Grid::Grid(int width, int height) : width(width), height(height)
{
    offsetX = 0;
    offsetY = 0;

    data = new int[width * height];

    for(int i = 0; i < width * height; i++)
        data[i] = 0;
}

int Grid::getValue(int x, int y)
{
    if(x < 0 || x >= width || y < 0 || y >= height) return 0;
    return data[y * width + x];
}

void Grid::setValue(int x, int y, int value)
{
    if(x < 0 || x >= width || y < 0 || y >= height) return;
    data[y * width + x] = value;
}

int Grid::getWidth()
{
    return width;
}

int Grid::getHeight()
{
    return height;
}

int Grid::getOffsetX()
{
    return offsetX;
}

int Grid::getOffsetY()
{
   return offsetY;
}

void Grid::setOffset(int x, int y)
{
    offsetX = x;
    offsetY = y;
}

void Grid::setOffsetX(int x)
{
    offsetX = x;
}

void Grid::setOffsetY(int y)
{
    offsetY = y;
}

void Grid::clear()
{
    for(int i = 0; i < width * height; i++)
        data[i] = 0;
}
