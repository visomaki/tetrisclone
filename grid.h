#ifndef GRID_H
#define GRID_H

/*
Simple base class to manage grid based data
*/

class Grid
{
public:
    Grid(int width, int height);

    int getValue(int x, int y);
    void setValue(int x, int y, int value);

    int getOffsetX();
    int getOffsetY();
    void setOffset(int x, int y);
    void setOffsetX(int x);
    void setOffsetY(int y);

    int getWidth();
    int getHeight();

    void clear();

protected:
    int* data;
    int width;
    int height;

    int offsetX;
    int offsetY;
};

#endif // GRID_H
