#include "gamegrid.h"
#include <QtGlobal>
#include "fallingpiece.h"

GameGrid::GameGrid() : Grid(GRID_WIDTH, GRID_HEIGHT)
{
}

void GameGrid::removeFullRows()
{
    for(int i = this->getHeight()-1; i >= 0; i--) {
        while(isRowFull(i)) {
            for(int x = 0; x < this->getWidth(); x++) {
                for(int y = i; y > 0; y--) {
                    this->setValue(x, y, this->getValue(x, y-1));
                }
            }
        }
    }
}

int GameGrid::getFullRowCount()
{
    int count = 0;

    for(int i = 0; i < this->getHeight(); i++)
        if(isRowFull(i)) count++;

    return count;
}

bool GameGrid::isRowFull(int idx)
{
    for(int i = 0; i < this->getWidth(); i++)
        if(this->getValue(i, idx) == 0) return false;

    return true;
}

bool GameGrid::isPieceOverlapping(FallingPiece* piece)
{
    //Check if grid is overlapping with a falling piece
    for(int x = 0; x < piece->getWidth(); x++) {
        for(int y = 0; y < piece->getHeight(); y++) {
            if(this->getValue(x + piece->getOffsetX(), y + piece->getOffsetY()) > 0 && piece->getValue(x, y) > 0)
                return true;
        }
    }

    return false;
}

void GameGrid::addPiece(FallingPiece* piece)
{
    //Copy the values of a piece to the data
    for(int x = 0; x < piece->getWidth(); x++) {
        for(int y = 0; y < piece->getHeight(); y++) {
            int value = piece->getValue(x, y);
            if(value > 0)
                this->setValue(x + piece->getOffsetX(), y + piece->getOffsetY(), value);
        }
    }
}
