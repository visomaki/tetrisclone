#include "fallingpiece.h"
#include "gamegrid.h"
#include "PieceData.h"
#include <QGlobal.h>

FallingPiece::FallingPiece() : Grid(PIECE_WIDTH, PIECE_HEIGHT)
{
    generateRandom();
}

bool FallingPiece::checkBottom(GameGrid* grid)
{
    if(this->getOffsetY() + this->getHeight() < grid->getHeight()) return false;

    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            if(this->getOffsetY() + y >= grid->getHeight()) {
                if(this->getValue(x, y) > 0) return true;
            }
        }
    }

    return false;
}

bool FallingPiece::checkLeft(GameGrid* grid)
{
    if(this->getOffsetX() > 0) return false;

    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            if(this->getOffsetX() + x < 0) {
                if(this->getValue(x, y) > 0) return true;
            }
        }
    }

    return false;
}

bool FallingPiece::checkRight(GameGrid* grid)
{
    if(this->getOffsetX() + this->getWidth() < grid->getWidth()) return false;

    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            if(this->getOffsetX() + x >= grid->getWidth()) {
                if(this->getValue(x, y) > 0) return true;
            }
        }
    }

    return false;
}

void FallingPiece::generateRandom()
{
    int type = qrand() % 7;

    for(int i = 0; i < getWidth() * getHeight(); i++)
        data[i] = pieces[type][i] * (type+1);
}

void FallingPiece::rotateLeft()
{
    int temp[PIECE_WIDTH * PIECE_HEIGHT];
    memcpy(temp, this->data, sizeof(int) * PIECE_WIDTH * PIECE_HEIGHT);

    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            data[y * PIECE_WIDTH + x] = temp[x * PIECE_WIDTH + (PIECE_HEIGHT-y-1)];
        }
    }
}

void FallingPiece::rotateRight()
{
    int temp[PIECE_WIDTH * PIECE_HEIGHT];
    memcpy(temp, this->data, sizeof(int) * PIECE_WIDTH * PIECE_HEIGHT);

    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            data[y * PIECE_WIDTH + x] = temp[(PIECE_WIDTH-x-1) * PIECE_WIDTH + y];
        }
    }
}

void FallingPiece::copy(FallingPiece* other)
{
    for(int x = 0; x < this->getWidth(); x++) {
        for(int y = 0; y < this->getHeight(); y++) {
            data[y * PIECE_WIDTH + x] = other->getValue(x, y);
        }
    }
}


