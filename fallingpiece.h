#ifndef FALLINGPIECE_H
#define FALLINGPIECE_H

#include "grid.h"

class GameGrid;

const int PIECE_WIDTH = 5;
const int PIECE_HEIGHT = 5;

/*
Class to manage a single falling piece (tetromino)
*/

class FallingPiece : public Grid
{
public:
    FallingPiece();

    bool checkBottom(GameGrid* grid);
    bool checkLeft(GameGrid* grid);
    bool checkRight(GameGrid* grid);

    void rotateRight();
    void rotateLeft();

    void generateRandom();

    void copy(FallingPiece* other);
};

#endif // FALLINGPIECE_H
