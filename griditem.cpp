#include "griditem.h"
#include <QtGui>

GridItem::GridItem(Grid* grid)
{
    setGrid(grid);

    colors = new QColor[7];
    colors[0].setRgb(255, 0, 0);
    colors[1].setRgb(0, 255, 0);
    colors[2].setRgb(0, 0, 255);
    colors[3].setRgb(255, 255, 0);
    colors[4].setRgb(0, 255, 255);
    colors[5].setRgb(255, 0, 255);
    colors[6].setRgb(255, 128, 0);
}

void GridItem::setGrid(Grid* grid)
{
    this->grid = grid;
}

void GridItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if(!grid) return;

    //Calculate offset in graphics
    int offsetX = BLOCK_WIDTH * grid->getOffsetX();
    int offsetY = BLOCK_HEIGHT * grid->getOffsetY();

    //Draw whole grid
    for(int x = 0; x < grid->getWidth(); x++) {
        for(int y = 0; y < grid->getHeight(); y++) {
            int value = grid->getValue(x, y);
            if(value > 0 && value <= 7) {
                painter->setBrush(colors[value-1]);
                painter->drawRect(QRect(30 * x + offsetX, 30 * y + offsetY, 30, 30));
            }
        }
    }
}


QRectF GridItem::boundingRect() const
{
    return QRectF(0, 0, 0, 0);
}

