#ifndef GAMEGRID_H
#define GAMEGRID_H

#include "grid.h"

class FallingPiece;

/*
Class to manage the "game board"
*/

const int GRID_WIDTH = 10;
const int GRID_HEIGHT = 20;

class GameGrid : public Grid
{
public:
    GameGrid();

    void removeFullRows();
    int getFullRowCount();

    bool isPieceOverlapping(FallingPiece* piece);
    void addPiece(FallingPiece* piece);

private:
    bool isRowFull(int idx);
};

#endif // GAMEGRID_H
